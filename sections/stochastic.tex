% !TeX root = ../main.tex
\documentclass[main.tex]{subfiles}
\begin{document}

\section{Основы случайных процессов}
\label{sec:stochastic_basics}

\subsection{Марковские процессы с дискретными состояниями и дискретным временем. Марковские цепи.}~
Пусть $S=\{S_1,S_2,\dots\}$ - множество состояний.
$S$ может быть построено по-разному в зависимости от требуемой степени детализации.
Система может переходить из состояния в состояние напрямую, либо через другое состояние.

\emph{Пример:}
\begin{itemize}
    \item $S_1$ - отличник
    \item $S_2$ - хорошист
    \item $S_3$ - троечник
    \item $S_4$ - хвостист
\end{itemize}

\begin{figure}[h]
	\centering
	\begin{tikzpicture}[->,node distance=2.5cm, semithick]
		\node[circle,draw](S1)			   {$S_1$};
		\node[circle,draw](S2)[right of=S1]{$S_2$};
		\node[circle,draw](S3)[below of=S1]{$S_3$};
		\node[circle,draw](S4)[right of=S3]{$S_4$};
        
        \path (S1) edge [loop left ] (S1);
        \path (S1) edge [bend right] (S2);
        \path (S2) edge [bend right] (S1);
        \path (S2) edge [loop right] (S2);

        \path (S2) edge (S4);
        \path (S2) edge (S3);
        \path (S3) edge (S4);
        \path (S1) edge (S3);
        \path (S3) edge [loop left ] (S3);
        \path (S4) edge [loop right] (S4);
	\end{tikzpicture}
	\caption{Пример случайного процесса с дискретным состоянием.}
\end{figure}~
\emph{Предположение.}
Переход между состояниями мгновенный и $\forall t:$ система находится в \underline{одном} состоянии.

\subsubsection{Классификация состояний.}
\begin{enumerate}
    \item Состояние-источник (все стрелки выходят)
    \item Поглощающее состояние (ни одна стрелка не выходит)
    \item $S_j$ соседнее по отношению к $S_i$, если есть переход $S_i\rightarrow S_j$
    \item Состояния $S_j$ и $S_i$ соседние, если $\exists S_i\rightarrow S_j \wedge \exists S_j\rightarrow S_i$
    \item Состояние транзитивно, если в него можно войти и из него можно выйти
    \item Состояние изолировано, если в него нельзя войти и из него нельзя выйти
\end{enumerate}
\subsubsection{Классификация подмножеств.}
\begin{enumerate}
    \item Подмножество называется замкнутым (кольцевым), если из него нельзя выйти
    \item Подмножество называется связанным, если из любого состояния можно попасть в любое
    \item Подмножество называется транзитивным, если в него можно попасть, а из него выйти
    \item Подмножество называется поглощающим, если в него можно попасть, а из него нельзя выйти
\end{enumerate}
\subsubsection{Стационарный режим.}~
При наличии стационарного режима после большого числа шагов распределение состояний не зависит от начального распределения.
Т.е. при описании процесса матрицей переходов $\pmb{P}=\{p_{ij}\}|\sum\limits_jp_{ij}=1$, где $p_{ij}$ - вероятность перехода системы из $S_i$ в $S_j$.
\[\lim_{n\rightarrow\infty}\pmb{P}^n=\begin{pmatrix}
    \vec P_1\\
    \vec P_2\\
    \vdots\\
\end{pmatrix}; \vec P_1=\vec P_2=\dots\]
\textbf{Условие существования стационарного режима}
\begin{enumerate}
    \item Множество состояний системы должно быть эргодическим (из каждого состояния можно перейти в каждое)
    \item Цепь Маркова должна быть однородной (матрица переходов постоянна и не зависит от времени)
    \item Цепь Маркова не должна быть циклической
\end{enumerate}~
\emph{Подмножество} цепи Маркова называется \emph{циклическим} если
\[\exists n\neq\infty:\forall i:\vec{p}_i\pmb{P}^n=\vec{p}_i,\]
где $\vec p_i$ - вектор распределения состояний на шаге $i$, а $n$ - циклическое число подмножества.

\subsubsection{Нахождение финальных вероятностей.}~
Финальное распределение вероятностей в стационарном режиме является инвариантом относительно оператора переходов.
Таким образом его можно найти как решение системы уравнений
\[\begin{cases}
    \vec\pi\pmb{P}=\vec\pi\\
    \sum_n\pi_i=1
\end{cases}\]
где 
\[\vec\pi=\begin{pmatrix}
    \pi_1 &\pi_2 &\dots &\pi_n
\end{pmatrix}\]

\subsubsection{Поглощающие состояния.}~
При наличии в сети Маркова поглощающих состояний не имеет смысла задача о распределении стационарных вероятностей,
однако имеет смысл задача о среднем числе шагов до попадения в поглощающее состояние.
Если обозначить как $\pmb Q$ подматрицу, соответствующую всем состояниям за исключением поглощающего,
то будет верно следующее соотношение
\[\vec l=\vec 1+\pmb Q\vec l\]
что в общем виде можно решить как
\[\vec l=(\pmb E-\pmb Q)^{-1}\vec 1\]

\subsection{Марковские процессы с дискретными состояниями и непрерывным временем.}~
Случайный процесс с дискретными состояниями и непрерывным временем называется Марковским, если $\forall t_0.t>t_0: P(t)$
не зависит от $S(t'):t'<t_0$, а только от $S(t_0)$.

Интенсивность перехода из состояния $i$ в состояние $j$ обозначается как $\lambda_{ij}(t)$, а
\[P_{ij}(t,t+\Delta t)=\lambda_{ij}(t)\Delta t\]

\emph{Предположение}

Все потоки Пуассоновские и независимые. Тогда мы имеем дело с Марковским процессом с дискретными состояниями и непрерывным временем.

Пусть дан набор состояний $S=\{S_1\dots S_n\}$ с интенсивностями потоков $\lambda_{ij}(t)$,
при этом $\lambda_{ii}(t)=0$. Тогда
\begin{gather}
    P(S(t+\Delta t)=j)=\sum\limits_{j\neq i}P(S(t)=i)\lambda_{ij}(t)\Delta t+P(S(t)=j)(1-\sum\limits_{i\neq j}\lambda_{ji}(t)\Delta t)\\
    P(S(t+\Delta t)=j)-P(S(t)=j)=\bigg(\sum\limits_{j\neq i}P(S(t)=i)\lambda_{ij}(t)-P(S(t)=j)\sum\limits_{i\neq j}\lambda_{ji}(t)\bigg)\Delta t\\
    \dfrac{P(S(t+\Delta t)=j)-P(S(t)=j)}{\Delta t}=\sum\limits_{j\neq i}P(S(t)=i)\lambda_{ij}(t)-P(S(t)=j)\sum\limits_{i\neq j}\lambda_{ji}(t)\\
    \lim_{\Delta t \rightarrow 0} \dfrac{P(S(t+\Delta t)=j)-P(S(t)=j)}{\Delta t} = \dfrac{\dd P_j(t)}{\dd t}\\
    \dfrac{\dd P_j(t)}{\dd t}=\sum\limits_{j\neq i}P_i\lambda_{ij}(t)-P_j\sum\limits_{i\neq j}\lambda_{ji}(t)
\end{gather}
Данная система называется СДУ Колмогорова и используется для нахождения распределения вероятностей состояний системы.
Если добавить к ней условие нормировки, то получится следующая система
\[\begin{cases}
    \dfrac{\dd P_j(t)}{\dd t}=\sum\limits_{j\neq i}P_i\lambda_{ij}(t)-P_j\sum\limits_{i\neq j}\lambda_{ji}(t)\\
    \forall t.\sum\limits_{i=1}^nP_i(t)=1
\end{cases}\]

\end{document}
